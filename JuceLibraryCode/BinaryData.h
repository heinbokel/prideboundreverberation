/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#pragma once

namespace BinaryData
{
    extern const char*   PolyLion_png;
    const int            PolyLion_pngSize = 304973;

    extern const char*   polyLion2_png;
    const int            polyLion2_pngSize = 75936;

    extern const char*   prideboundAudioLogo_png;
    const int            prideboundAudioLogo_pngSize = 53771;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Number of elements in the namedResourceList array.
    const int namedResourceListSize = 3;

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes) throw();
}
